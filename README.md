# desafio-analise-credito

## Rodando o projeto

### Banco de dados

O banco usado foi MySQL. Deve ser criado um DB com as seguintes configurações:

    DATABASE_HOST: localhost
    DATABASE_PORT: 3306
    DATABASE_NAME: strapi

E um usuario com acesso ao DB e as seguintes credenciais

    DATABASE_USERNAME: strapi
    DATABASE_PASSWORD: strapi

No email esta disponivel um dump file com multiplas entradas ja cadastradas para facilitar a demonstração.

### Backend

O backend foi escrito em nodejs e usa um CMS chamado Strapi, as configurações do CMS vão estar junto com o DUMP do DB enviado por email.

    cd backend
    npm install
    npm start

O server deve estar disponivel na URL http://localhost:1337

O playground do GraphQL deve estar disponivel em http://localhost:1337/graphql (para usar é necessário se autenticar e incluir o token nos headers)

### Frontend

    cd frontend
    npm install
    ng serve

## Sobre o projeto

### O que foi feito

* O usuario pode se logar na aplicação, usuarios não logados devem ser mandados de volta para a tela de login se tentarem acessar paginas internas.
* A autenticação usa JWT e o token é mantido no localstorage
* Existem 2 roles (captador e analista)
    * O captador pode acessar a pagina /portadores e ver a lista de portadores cadastrados por ele
    * O captador pode acessar a pagina /novo-portador para cadastrar um novo portador
    * O analista pode acessar a pagina de /portadores e ver a lista de portadores que ainda tem status pendente
    * O analista pode clicar em um portador para ver mais detalhes sobre ele e pode alterar seu status para "aprovado" ou "negado", dependendo de sua analise
    * O captador não tem acesso a pagina de detalhes do portador
    * O analista não tem acesso a pagina de cadastro de portador

### Como acessar 

No DB ja estão cadastrados alguns usuarios para testes:

    usuario: captador   
    senha: Senha123

    usuario: captador2   
    senha: Senha123

    usuario: analista   
    senha: Senha123


### Comentários

O projeto foi interessante, acabou sendo um verdadeiro desafio pois já fazia algum tempo que eu não usava Angular e ele mudou bastante desde então.

Tive que sacrificar um pouco na parte de validação do formulário e na filtragem de dados na tabela para que conseguisse fazer bem a parte de autenticação e autorização, então são essas algumas coisas que eu ainda poderia melhorar no projeto.

Acho que acabei conseguindo fazer um trabalho legal e razoavelmente organizado para o tempo que tive, que dividi entre estudo e desenvolvimento, estou entregando com um pouco de atraso mas de qualquer maneira aproveitei o processo e a chance de relembrar o framework. Obrigado pela oportunidade.

