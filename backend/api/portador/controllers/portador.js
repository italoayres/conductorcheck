const { parseMultipartData, sanitizeEntity } = require('strapi-utils');

module.exports = {
  /**
   * Update a record.
   *
   * @return {Object}
   */

  async update(ctx) {
    const { id } = ctx.params;

    const user = ctx.state.user;
    const role = ctx.state.user.role.name;

    let entity;
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.portador.update(
        { id },
        { status: data.status, analista: user.id },
        { files, }
      );
    } else {
      entity = await strapi.services.portador.update({ id }, ctx.request.body);
    }

    return sanitizeEntity(entity, { model: strapi.models.portador });
  },

  async find(ctx) {
    let entities;
    const user = ctx.state.user;
    const role = ctx.state.user.role.name;

    // Analista so ve portadores pendentes
    if (role == 'analista') {
      ctx.query = {
        ...ctx.query,
        status: 'pendente',
      }
    }

    // Captador so ve os proprios portadores
    if (role == 'captador') {
      ctx.query = {
        ...ctx.query,
        captador: user.id
      }
    }

    if (ctx.query._q) {
      entities = await strapi.services.portador.search(ctx.query);
    } else {
      entities = await strapi.services.portador.find(ctx.query);
    }

    return entities.map(entity => sanitizeEntity(entity, { model: strapi.models.portador }));
  },

  async create(ctx) {
    let entity;
    const user = ctx.state.user;
    const role = ctx.state.user.role.name;

    
    if (ctx.is('multipart')) {
      const { data, files } = parseMultipartData(ctx);
      data.captador = user.id
      data.score = String(Math.round(Math.random() * 1000))
      
      entity = await strapi.services.portador.create(data, { files });
    } else {
      
      ctx.request.body.captador = user.id
      ctx.request.body.score = String(Math.round(Math.random() * 1000))
      entity = await strapi.services.portador.create(ctx.request.body);
    }
    return sanitizeEntity(entity, { model: strapi.models.portador });
  },
};
