import gql from "graphql-tag";

const CREATE_PORTADOR_MUTATION = gql`
mutation CreatePortador($input: createPortadorInput) {
    createPortador(input: $input) {
      portador {
        id
      }
    }
  }
`;

export default CREATE_PORTADOR_MUTATION;