import gql from "graphql-tag";

const UPDATE_PORTADOR_MUTATION = gql`
mutation UpdateStatus($input: updatePortadorInput) {
    updatePortador(input: $input) {
      portador {
        id
        status
      }
    }
  }
`;

export default UPDATE_PORTADOR_MUTATION;