import gql from "graphql-tag";

const USER_LOGIN_MUTATION = gql`
mutation UserLogin($input: UsersPermissionsLoginInput!) {
    login(input: $input) {
        jwt
        user {
            id
            username
            role {
                name
            }
        }
      
    }
}
`;

export default USER_LOGIN_MUTATION;