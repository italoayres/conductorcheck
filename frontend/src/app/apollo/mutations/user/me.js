import gql from "graphql-tag";

const USER_INFO_MUTATION = gql`
query UserInfo {
  me {
    id
    username
    role {
      name
    }
  }
}
`;

export default USER_INFO_MUTATION;