import gql from "graphql-tag";

const COUNT_ARTICLES_QUERY = gql`
query {
    portadorsConnection {
        aggregate {
          count
          totalCount
        }
    }
  }
`;

export default COUNT_ARTICLES_QUERY;