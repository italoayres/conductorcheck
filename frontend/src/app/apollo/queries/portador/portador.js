import gql from "graphql-tag";

const PORTADOR_QUERY = gql`
  query Portador($id: ID!) {
    portador(id: $id) {
        id
        created_at
        updated_at
        nome
        cpf
        rg
        dob
        renda
        antecedentes
        dividasPendentes
        phone
        estadoCivil
        escolaridade
        cargo
        endereco
        score
        info
        proposta
        captador {
          id
          username
          email
        }
        analista {
          id
          username
          email
        }
        status
    }
  }
`;

export default PORTADOR_QUERY;