import gql from "graphql-tag";

const PORTADORES_QUERY = gql`
  query Portadores {
    portadors {
        id
        created_at
        nome
        cpf
        captador {
          id
          username
          email
        }
        analista {
          id
          username
          email
        }
        status 
    }
  }
`;

export default PORTADORES_QUERY;