import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common'
import { BrowserModule } from '@angular/platform-browser';

// Pages
import { DataTableComponent } from './data-table/data-table.component';
import { LoginComponent } from './login/login.component';
import { NovoPortadorFormComponent } from './novo-portador-form/novo-portador-form.component';
import { ShowPortadorComponent } from './show-portador/show-portador.component';


// Auth components
import { AuthGuard } from "./shared/guard/auth.guard";
import { SecureInnerPagesGuard } from "./shared/guard/secure-inner-pages.guard";


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: 'portadores', component: DataTableComponent, canActivate: [AuthGuard] },
  { path: "portador/:id", component: ShowPortadorComponent, canActivate: [AuthGuard], data: { roles: ['analista'] } },
  { path: 'novo-portador', component: NovoPortadorFormComponent, canActivate: [AuthGuard], data: { roles: ['captador'] } },
  { path: 'login', component: LoginComponent, canActivate: [SecureInnerPagesGuard] },

];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }