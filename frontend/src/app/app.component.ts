import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', './theme/material.scss']
})
export class AppComponent {
  title = 'frontend';
}
