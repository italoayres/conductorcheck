import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';

import { NavComponent } from "./nav/nav.component";
import { DataTableComponent } from "./data-table/data-table.component";
import { NovoPortadorFormComponent } from './novo-portador-form/novo-portador-form.component';
import { LoginComponent } from './login/login.component';
import {ErrorInterceptor} from './shared/services/error.interceptor'

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxMaskModule, IConfig } from 'ngx-mask'

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    DataTableComponent,
    NovoPortadorFormComponent,
    LoginComponent
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    GraphQLModule,
    HttpClientModule,
    NgxDatatableModule,
    CommonModule,
    NgxMaskModule.forRoot(),
  ],
  providers: [
    ErrorInterceptor
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
