import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Portador } from './model/portador';
import { Page } from './model/page';

import { Apollo } from "apollo-angular";
import gql from "graphql-tag";
import PORTADORES_QUERY from "../apollo/queries/portador/portadores";
import COUNT_PORTADORES_QUERY from "../apollo/queries/portador/count_portadores";
import { Subscription } from "rxjs";
import {SelectionType, ColumnMode} from "@swimlane/ngx-datatable"

import { AuthService } from "../shared/services/auth.service"


// import { ColumnMode } from 'projects/swimlane/ngx-datatable/src/public-api';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})

export class DataTableComponent implements OnInit {
  @ViewChild('statusTmpl', { static: true }) statusTmpl: TemplateRef<any>;
  
  page = new Page();
  rows = new Array<Portador>();
  limit: number = 5;
  offset: number = 1;
  totalArticles:number;
  loading = true;
  data: any = {};
  errors: any;
  portadores: any;
  totalPortadores: any;
  columns: any[];
  
  SelectionType = SelectionType;
  ColumnMode = ColumnMode;
  selected = [];
  userData: any;  


  private queryPortadores: Subscription;
  private queryCountPortadores: Subscription;

  constructor(private apollo: Apollo, private router: Router, private authService: AuthService) {
    this.page.pageNumber = 0;
    this.page.size = 20;
  }

  ngOnInit() {
    this.userData = this.authService.me();

    this.columns = [
      { name: 'Id', prop: 'id' }, 
      { name: 'Nome', prop: 'nome' }, 
      { name: 'CPF', prop: 'cpf' }, 
      { name: 'Status', prop: 'status', cellTemplate: this.statusTmpl, }, 
    ]

    this.queryCountPortadores = this.apollo
    .watchQuery({
      query: COUNT_PORTADORES_QUERY,   
    })
    .valueChanges.subscribe(result => {
      this.totalPortadores = Object.values(result)[0]['portadorsConnection']['aggregate']['totalCount'];
      this.getAllPortadores(this.offset);

    });

    this.getAllPortadores(1);
  }



  getAllPortadores( offset:number ) {
    this.loading = true;
    this.offset = offset
    let start = (this.offset - 1) * this.limit
    this.queryPortadores = this.apollo
      .watchQuery({
        query: PORTADORES_QUERY,
        variables: {
          start: start,
          limit: this.limit
        }
      })
      .valueChanges.subscribe(result => {
        this.data = result.data;
        this.portadores = this.data.portadors;
        this.loading = result.loading;
        this.errors = result.errors;
      });
  }

  setPage(pageInfo) {
      this.getAllPortadores(pageInfo.offset)
  }

  ngOnDestroy() {
    this.queryPortadores.unsubscribe();
    this.queryCountPortadores.unsubscribe();
  }

  onSelect({ selected }) {
    let portadorSelecionado = selected[0];

    if(this.userData.role.name == 'analista') {
      this.router.navigate(['/portador/:id', {id: portadorSelecionado.id}]);

    }

  }

  onActivate(event) {
  }



}