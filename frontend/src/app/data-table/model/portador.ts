export class Portador {

    id: string;
    created_at: string;
    updated_at: string;
    nome: string;
    cpf: string;
    rg: string;
    dob: string;
    renda: string;
    antecedentes: string;
    dividasPendentes: string;
    phone: string;
    estadoCivil: string;
    escolaridade: string;
    cargo: string;
    endereco: string;
    score: string;
    info: string;
    proposta: string;
    status: string;

    constructor(
        id: string,
        created_at: string,
        updated_at: string,
        nome: string,
        cpf: string,
        rg: string,
        dob: string,
        renda: string,
        antecedentes: string,
        dividasPendentes: string,
        phone: string,
        estadoCivil: string,
        escolaridade: string,
        cargo: string,
        endereco: string,
        score: string,
        info: string,
        proposta: string,
        status: string
    ) {


        this.id = id;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.dob = dob;
        this.renda = renda;
        this.antecedentes = antecedentes;
        this.dividasPendentes = dividasPendentes;
        this.phone = phone;
        this.estadoCivil = estadoCivil;
        this.escolaridade = escolaridade;
        this.cargo = cargo;
        this.endereco = endereco;
        this.score = score;
        this.info = info;
        this.proposta = proposta;
        this.status = status;
    }
}