import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Apollo } from "apollo-angular";
import gql from "graphql-tag";
import NOVO_PORTADOR_MUTATION from "../apollo/mutations/portador/novo_portador";
import { Subscription } from "rxjs";

import { AuthService } from "../shared/services/auth.service"

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private auth: AuthService) {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.fb.group({
      usuario: ['captador', Validators.required],
      senha: ['Senha123', Validators.required], //this.fb.control({ value: null, disabled: false} ),//, GenericValidator.isValidCpf()),
    });
  }

  ngOnInit(): void {
  }

  onLoginSubmit(): void {
    let usuario: string = this.loginForm.value['usuario']
    let senha: string = this.loginForm.value['senha']

    console.log("submit", {usuario, senha})

    this.auth.login(usuario, senha)
  }

}
