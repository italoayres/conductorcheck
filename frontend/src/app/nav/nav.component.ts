import { Component, OnInit } from '@angular/core';

import { Apollo } from "apollo-angular";
import gql from "graphql-tag";
import { Subscription, Observable } from "rxjs";

import { AuthService } from "../shared/services/auth.service"


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})

export class NavComponent implements OnInit {
  userData: Observable<any>;
  isLoggedIn: Observable<boolean>;

  constructor(private apollo: Apollo, private authService: AuthService) {}

  ngOnInit() {
    this.authService.meObservable().subscribe((data) => {
      this.userData = data;
    });
    this.isLoggedIn = this.authService.isLoggedInAsObservable;
  }
  ngOnDestroy() {
  }

  handleLogout() {
    this.authService.logout();
  }

}
