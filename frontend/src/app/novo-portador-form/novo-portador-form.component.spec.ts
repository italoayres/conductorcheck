import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NovoPortadorFormComponent } from './novo-portador-form.component';

describe('NovoPortadorFormComponent', () => {
  let component: NovoPortadorFormComponent;
  let fixture: ComponentFixture<NovoPortadorFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NovoPortadorFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoPortadorFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
