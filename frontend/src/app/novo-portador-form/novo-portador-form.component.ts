import { Component } from '@angular/core';
import {Router} from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GenericValidator } from './cpfValidator'

import { Apollo } from "apollo-angular";
import gql from "graphql-tag";
import NOVO_PORTADOR_MUTATION from "../apollo/mutations/portador/novo_portador";
import PORTADORES_QUERY from "../apollo/queries/portador/portadores";

import { Subscription } from "rxjs";

import UIkit from 'uikit'


@Component({
  selector: 'app-novo-portador-form',
  templateUrl: './novo-portador-form.component.html',
  styleUrls: ['./novo-portador-form.component.css'],

})
export class NovoPortadorFormComponent {
  portadorForm: FormGroup;
  cpf_cnpj: string = '111.111.111-11';
  phoneNumber: string = '11111111111';

  constructor(private fb: FormBuilder, private apollo: Apollo, private router: Router) {
    this.createForm();
  }
  createForm() {
    this.portadorForm = this.fb.group({
      nome: ['Homer Simpson', Validators.required],
      cpf: ['111.111.111-11', Validators.required], //this.fb.control({ value: null, disabled: false} ),//, GenericValidator.isValidCpf()),
      dob: ['1965-12-21'],
      renda: [0],
      antecedentes: [false],
      dividasPendentes: [false],
      phone: this.fb.control({ value: null, disabled: false }),//['', [Validators.min(5), Validators.max(10)]],
      estadoCivil: ['solteiro'],
      escolaridade: ['analfabeto'],
      cargo: ['Operador', Validators.required],
      endereco: ['Rua Evergreen, Springfield', Validators.required],
      info: [''],
      proposta: ['R$ 5000,00', Validators.required],
    });
  }

  isValidNumber(): boolean {
    return this.phoneNumber.length > 9
  }

  isCPF(): boolean {
    return this.cpf_cnpj == null ? true : this.cpf_cnpj.length < 12 ? true : false;
  }

  getCpfCnpjMask(): string {
    return this.isCPF() ? '000.000.000-009' : '00.000.000/0000-00';
  }

  salvarPortador() {
    const input = {
      data: {
        nome: this.portadorForm.value['nome'],
        cpf: this.portadorForm.value['cpf'],
        dob: this.portadorForm.value['dob'],
        renda: Number(this.portadorForm.value['renda']),
        antecedentes: Boolean(this.portadorForm.value['antecedentes']),
        dividasPendentes: Boolean(this.portadorForm.value['dividasPendentes']),
        phone: this.portadorForm.value['phone'],
        estadoCivil: this.portadorForm.value['estadoCivil'],
        escolaridade: this.portadorForm.value['escolaridade'],
        cargo: this.portadorForm.value['cargo'],
        endereco: this.portadorForm.value['endereco'],
        info: this.portadorForm.value['info'],
        proposta: this.portadorForm.value['proposta']
      }
    }

    this.apollo
      .mutate({
        mutation: NOVO_PORTADOR_MUTATION,
        variables: {
          input,
        },
        refetchQueries: [{query: PORTADORES_QUERY}]
      })
      .subscribe(
        ({ data }) => {
          UIkit.notification("Portador cadastrado com sucesso", { pos: 'bottom-right', status: 'success' })
          this.router.navigate(['portadores']);
          
          
        },
        error => {
          UIkit.notification("Erro", { pos: 'bottom-right', status: 'danger' })
          console.log("there was an error sending the query", error);
        }
      );
  }

}