import { Injectable, NgZone } from '@angular/core';
import { User } from "./user";
import { Router } from "@angular/router";
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';

import { Apollo } from "apollo-angular";
import gql from "graphql-tag";
import USER_AUTH_MUTATION from "../../apollo/mutations/user/auth";
import { Subscription } from "rxjs";

import UIkit from 'uikit'


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  public authToken: string;
  
  private authApiBase: string = 'http://localhost:1337/graphql';
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  
  private loggedIn = new BehaviorSubject<boolean>(false);
  userData = new BehaviorSubject<any>(null); 

  constructor(
    private httpClient: HttpClient,
    public router: Router,
    public ngZone: NgZone, // NgZone service to remove outside scope warning,
    private apollo: Apollo
  ) {

    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();

    if (localStorage.getItem('currentUser')) {
      this.userData.next(JSON.parse(localStorage.getItem('currentUser')));
      this.authToken = localStorage.getItem('token');
    }
  }

  // Sign in with email/password
  login(identifier, password) {
    return this.apollo
      .mutate({
        mutation: USER_AUTH_MUTATION,
        variables: {
          input: {
            identifier,
            password
          },
        },
      })
      .subscribe(
        ({ data }: { data: any }) => {

          if (data.login && data.login.jwt && data.login.user) {

            localStorage.setItem('currentUser', JSON.stringify(data.login.user));
            localStorage.setItem('token', data.login.jwt);
            
            this.userData.next(data.login.user);
            this.authToken = localStorage.getItem('token');
            this.loggedIn.next(true);

            UIkit.notification("Login efetuado com sucesso", { pos: 'bottom-right', status: 'success' })
            this.router.navigate(['portadores']);
            
            
            return data.login.user;
          }
        },
        error => {
          UIkit.notification("Houve uma falha no login", { pos: 'bottom-right', status: 'danger' })
          console.log("there was an error sending the query", error);
        }
      );

  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    this.currentUserSubject.next(null);
    this.router.navigate(['login']);

    this.loggedIn.next(false);

    this.apollo.client.resetStore()

    UIkit.notification("O usuario foi deslogado.", { pos: 'bottom-right', status: 'primary' })
  }


  get isLoggedInAsObservable() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    this.loggedIn.next((user !== null) ? true : false);
    return this.loggedIn.asObservable();
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    return (user !== null) ? true : false;
  }

  meObservable() {
    if(this.userData) 
      return this.userData.asObservable();
    
    if (localStorage.getItem('currentUser')) {
      this.userData.next(JSON.parse(localStorage.getItem('currentUser')));
      return this.userData.asObservable();
    }


  }
  me() {
    if(this.userData) 
      return this.userData.value;
    
    if (localStorage.getItem('currentUser')) {
      this.userData.next(JSON.parse(localStorage.getItem('currentUser')));
      return this.userData.value;
    }


  }


}