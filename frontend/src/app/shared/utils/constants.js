const mapEscolaridade={
    "analfabeto":"Analfabeto",
    "ensino_fundamental_incompleto":"Ensino fundamental incompleto",
    "ensino_fundamental_completo":"Ensino fundamental completo",
    "ensino_medio_incompleto":"Ensino medio incompleto",
    "ensino_medio_completo":"Ensino medio completo",
    "superior_incompleto":"Superior incompleto",
    "superior_completo":"Superior completo",
    "mestrado":"Mestrado",
    "doutorado":"Doutorado",
  }
const mapRenda={
    0:"Não possui",
    1:"Até R$ 1000,00",
    2:"Até R$ 3000,00",
    3:"Até R$ 5000,00",
    4:"Até R$ 1000,00",
    5:"Acima de R$ 10000,00",
  }

export {mapEscolaridade, mapRenda}