import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPortadorComponent } from './show-portador.component';

describe('ShowPortadorComponent', () => {
  let component: ShowPortadorComponent;
  let fixture: ComponentFixture<ShowPortadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowPortadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPortadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
