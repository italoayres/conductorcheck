import { Component, OnInit } from "@angular/core";
import { Apollo } from "apollo-angular";
import gql from "graphql-tag";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import {Router} from '@angular/router'

import { Portador } from "../data-table/model/portador";
import PORTADOR_QUERY from "../apollo/queries/portador/portador";
import PORTADORES_QUERY from "../apollo/queries/portador/portadores";
import UPDATE_PORTADOR_MUTATION from "../apollo/mutations/portador/update_status";

import {mapEscolaridade, mapRenda} from '../shared/utils/constants'

import UIkit from 'uikit'


@Component({
  selector: 'app-show-portador',
  templateUrl: './show-portador.component.html',
  styleUrls: ['./show-portador.component.css']
})
export class ShowPortadorComponent implements OnInit {
  data: any = {};
  loading = true;
  errors: any;

  mapEscolaridade = mapEscolaridade;
  mapRenda = mapRenda;

  private queryPortador: Subscription;
  portador: Portador;

  constructor(private apollo: Apollo, private route: ActivatedRoute, private router: Router) {}
  paramId: string;

  ngOnInit() {
    this.paramId = this.route.snapshot.paramMap.get("id")

    this.queryPortador = this.apollo
      .watchQuery({
        query: PORTADOR_QUERY,
        variables: {
          id: this.paramId
        }
      })
      .valueChanges.subscribe(result => {
        this.data = result.data;
        this.loading = result.loading;
        this.errors = result.errors;

        this.portador = this.data.portador;
      });
  }
  ngOnDestroy() {
    this.queryPortador.unsubscribe();
  }

  buttonClickAprovar() {
    this.updateStatus("aprovado")
  }

  buttonClickReprovar() {
    this.updateStatus("negado")

  }

  updateStatus(status) {


    this.apollo
    .mutate({
      mutation: UPDATE_PORTADOR_MUTATION,
      variables: {
        input: {
          where: {
            id: this.paramId
          },
          data: {
            status
          }
        }
        
      },
      refetchQueries: [{query: PORTADORES_QUERY}]
    })
    .subscribe(result => {
      this.data = result.data;
      this.errors = result.errors;

      if(result.errors) {
        UIkit.notification("Houve um erro ao atualizar o portador", { pos: 'bottom-right', status: 'danger' })

      } else if(this.data.updatePortador) {
        UIkit.notification("Portador atualizado com sucesso", { pos: 'bottom-right', status: 'success' })

        this.router.navigate(['/portadores'])
      }         

    });
  }
}